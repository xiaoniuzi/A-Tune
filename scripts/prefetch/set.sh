#!/bin/sh
# Copyright (c) 2019 Huawei Technologies Co., Ltd.
# A-Tune is licensed under the Mulan PSL v1.
# You can use this software according to the terms and conditions of the Mulan PSL v1.
# You may obtain a copy of Mulan PSL v1 at:
#     http://license.coscl.org.cn/MulanPSL
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
# PURPOSE.
# See the Mulan PSL v1 for more details.
# Create: 2019-10-29

SCRIPT=$(basename $0)
SCRIPT_PATH=$(cd "$(dirname "$0")";pwd)

if [ $# != 1 ]; then
	echo "Usage: ${SCRIPT} on or off"
	exit 1
fi

lsmod | grep prefetch_tuning &> /dev/null
uninstall=$?

case "$1" in
	"on")
		policy=15
		;;
	"off")
		policy=0
		;;
	"reset")
		if [ ${uninstall} = 0 ]; then
			rmmod prefetch_tuning
		fi
		exit 0
		;;
	*)
		exit 2
		;;
esac

if [ ${uninstall} != 0 ]; then
	modprobe prefetch_tuning
	[ $? != 0 ] && exit 3
fi

echo ${policy} > /sys/class/misc/prefetch/policy
[ $? != 0 ] && exit 4

exit 0
